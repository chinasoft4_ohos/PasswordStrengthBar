# PasswordStrengthBar

#### 项目介绍
- 项目名称：PasswordStrengthBar
- 所属系列：openharmony的第三方组件适配移植
- 功能：通过分隔的强度条查看密码的强度。
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release 1.2.0

#### 效果演示
<img src="img/gif.gif"></img>

#### 安装教程

1.在项目根目录下的build.gradle文件中，
 ```
 allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
 }
```
2.在entry模块的build.gradle文件中，
 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:PasswordStrengthBar:1.0.0')
    ......
 }
```
在sdk6，DevEco Studio 2.2 beta1下项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

1、您可以添加到您的xml布局：
 ```
 <com.ohos.thenishchalraj.passwordstrength.PasswordStrengthBar
         ohos:id="$+id:passwordBarCheck"
         ohos:height="4vp"
         ohos:width="match_parent"
         ohos:top_margin="6vp"
         ohos:right_margin="16vp"
         ohos:left_margin="16vp"/>
```
2、在MainAbilitySlice中找到控件。
```
  PasswordStrengthBar passwordStrengthBar = (PasswordStrengthBar) findComponentById(ResourceTable.Id_passwordBarCheck);
  PasswordStrengthBar passwordField = (TextField) findComponentById(ResourceTable.Id_passwordFieldCheck);
  passwordStrengthBar.setStrengthColor(-3355444, mColor1, mColor2, mColor3, mColor4);
  passwordField.addTextObserver(new Text.TextObserver() {

      @Override
      public void onTextUpdated(String str, int i, int i1, int i2) {
          calculation(str);
          if (str.isEmpty()) {
              see.setVisibility(Component.INVISIBLE);
          } else {
              see.setVisibility(Component.VISIBLE);
          }
      }
  });
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### 版权和许可信息

- Apache License Version 2.0