/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ohos.thenishchalraj.passwordstrengthbar.slice;

import com.ohos.thenishchalraj.passwordstrength.PasswordStrengthBar;
import com.ohos.thenishchalraj.passwordstrengthbar.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.InputAttribute;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;

/**
 * MainAbilitySlice
 *
 * @author ljx
 * @since 2021-07-08
 */
public class MainAbilitySlice extends AbilitySlice {

    private PasswordStrengthBar passwordStrengthBar;
    private TextField passwordField;
    private Text check;
    private Button see;
    private int mColor1;
    private int mColor2;
    private int mColor3;
    private int mColor4;
    private boolean isShown = true;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        this.mColor1 = getColor(ResourceTable.Color_colorRed);
        this.mColor2 = getColor(ResourceTable.Color_colorOrange);
        this.mColor3 = getColor(ResourceTable.Color_colorLightGreen);
        this.mColor4 = getColor(ResourceTable.Color_colorDarkGreen);
        passwordStrengthBar = (PasswordStrengthBar) findComponentById(ResourceTable.Id_passwordBarCheck);
        passwordField = (TextField) findComponentById(ResourceTable.Id_passwordFieldCheck);
        check = (Text) findComponentById(ResourceTable.Id_strengthText);
        see = (Button) findComponentById(ResourceTable.Id_visibilityButton);
        passwordStrengthBar.setStrengthColor(mColor1, mColor1, mColor2, mColor3, mColor4);
        passwordField.addTextObserver(new Text.TextObserver() {

            @Override
            public void onTextUpdated(String str, int ii, int i1, int i2) {
                calculation(str);
                if (str.isEmpty()) {
                    see.setVisibility(Component.INVISIBLE);
                } else {
                    see.setVisibility(Component.VISIBLE);
                }
            }
        });
        see.setClickedListener(v -> {
            if (isShown) {
                isShown = false;
                see.setText("Hide Password");
                passwordField.setTextInputType(InputAttribute.PATTERN_TEXT);
                getUITaskDispatcher().delayDispatch(new Runnable() {
                    @Override
                    public void run() {
                        isShown = true;
                        see.setText("See Password");
                        passwordField.setTextInputType(InputAttribute.PATTERN_PASSWORD);
                    }
                }, 2000);
            } else {
                isShown = true;
                see.setText("See Password");
                passwordField.setTextInputType(InputAttribute.PATTERN_PASSWORD);
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private void calculation(String data) {
        int uppercase = 0;
        int lowercase = 0;
        int digits = 0;
        int symbols = 0;
        int bonus = 0;
        int requirements = 0;
        int lettersOnly = 0;
        int numbersOnly = 0;
        int cuc = 0;
        int clc = 0;
        int length = data.length();
        int kk = 0;
        int ll;
        for (ll = ((CharSequence) data).length(); kk < ll; ++kk) {
            if (Character.isUpperCase(data.charAt(kk))) {
                ++uppercase;
            } else if (Character.isLowerCase(data.charAt(kk))) {
                ++lowercase;
            } else if (Character.isDigit(data.charAt(kk))) {
                ++digits;
            }
            symbols = length - uppercase - lowercase - digits;
        }
        kk = 1;
        for (ll = data.length() - 1; kk < ll; ++kk) {
            if (Character.isDigit(data.charAt(kk))) {
                ++bonus;
            }
        }
        calculation1(kk, data, cuc);
        calculation2(ll, data, clc);
        setRequire(length, requirements);
        setRequirements(uppercase, requirements);
        setRequirements(lowercase, requirements);
        setRequirements(digits, requirements);
        setRequirements(symbols, requirements);
        setRequirements(bonus, requirements);
        if (digits == 0 && symbols == 0) {
            lettersOnly = 1;
        }
        if (lowercase == 0 && uppercase == 0 && symbols == 0) {
            numbersOnly = 1;
        }
        int total = length * 4 + (length - uppercase) * 2 + (length - lowercase) * 2
                + digits * 4 + symbols * 6 + bonus * 2 + requirements * 2 -
                lettersOnly * length * 2 - numbersOnly * length * 3 - cuc * 2 - clc * 2;
        setPasswordStrengthBar(total);
    }

    private void calculation1(int kk, String data, int cuc) {
        for (kk = 0; kk < data.length(); ++kk) {
            if (Character.isUpperCase(data.charAt(kk))) {
                ++kk;
                if (kk < data.length() && Character.isUpperCase(data.charAt(kk))) {
                    ++cuc;
                    --kk;
                }
            }
        }
    }

    private void calculation2(int ll, String data, int clc) {
        for (ll = 0; ll < data.length(); ++ll) {
            if (Character.isLowerCase(data.charAt(ll))) {
                ++ll;
                if (ll < data.length() && Character.isLowerCase(data.charAt(ll))) {
                    ++clc;
                    --ll;
                }
            }
        }
    }

    private void setRequirements(int value, int requirements) {
        if (value > 0) {
            ++requirements;
        }
    }

    private void setRequire(int length, int requirements) {
        if (length > 7) {
            ++requirements;
        }
    }

    private void setPasswordStrengthBar(int total) {
        if (total >= 1 && total <= 50) {
            check.setText("BAD");
            passwordStrengthBar.setStrength(total / 2);
            return;
        } else if (total >= 51 && total <= 70) {
            check.setText("AVERAGE");
            passwordStrengthBar.setStrength(total * 5 / 7);
            return;
        } else if (total >= 71 && total <= 80) {
            check.setText("GOOD");
            passwordStrengthBar.setStrength(total * 7 / 8);
            return;
        } else if (total >= 81) {
            check.setText("BEST");
            passwordStrengthBar.setStrength(total);
            return;
        } else {
            check.setText("");
            passwordStrengthBar.setStrength(passwordStrengthBar.getMinStrength());
        }
    }
}