/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.ohos.thenishchalraj.passwordstrengthbar;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;

import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;

/**
 * 测试
 *
 * @author ljx
 * @since 2021-07-08
 */
public class ExampleOhosTest {
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.ohos.thenishchalraj.passwordstrengthbar", actualBundleName);
    }

    @Test
    public void getMaxStrength() {
        try {
            Class  mainAbilitySlice = Class.forName("com.ohos.thenishchalraj.passwordstrength.PasswordStrengthBar");
            Method log = mainAbilitySlice.getMethod("getMaxStrength");
            Object obj = mainAbilitySlice.getConstructor().newInstance();
            log.invoke(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getMinStrength() {
        try {
            Class  mainAbilitySlice = Class.forName("com.ohos.thenishchalraj.passwordstrength.PasswordStrengthBar");
            Method log = mainAbilitySlice.getMethod("getMinStrength");
            Object obj = mainAbilitySlice.getConstructor().newInstance();
            log.invoke(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getStrength() {
        try {
            Class  mainAbilitySlice = Class.forName("com.ohos.thenishchalraj.passwordstrength.PasswordStrengthBar");
            Method log = mainAbilitySlice.getMethod("getStrength");
            Object obj = mainAbilitySlice.getConstructor().newInstance();
            log.invoke(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getmNoStrengthColor() {
        try {
            Class  mainAbilitySlice = Class.forName("com.ohos.thenishchalraj.passwordstrength.PasswordStrengthBar");
            Method log = mainAbilitySlice.getMethod("getmNoStrengthColor");
            Object obj = mainAbilitySlice.getConstructor().newInstance();
            log.invoke(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}