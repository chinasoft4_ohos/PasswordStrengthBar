package com.ohos.thenishchalraj.passwordstrength;

import ohos.agp.components.AttrSet;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ProgressBar;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * Created by thenishchalraj on 10.10.2018.
 */

public class PasswordStrengthBar extends DirectionalLayout {

    protected DirectionalLayout plb;
    protected ProgressBar pb1;
    protected ProgressBar pb2;
    protected ProgressBar pb3;
    protected ProgressBar pb4;

    private int mMax = 100;
    private int mMin = 0;
    private int mNoStrengthColor = Color.LTGRAY.getValue();
    private int mStrengthColor1 = Color.RED.getValue();
    private int mStrengthColor2 = Color.YELLOW.getValue();
    private int mStrengthColor3 = Color.GREEN.getValue();
    private int mStrengthColor4 = Color.DKGRAY.getValue();

    public PasswordStrengthBar(Context context) {
        super(context);
        init(context);
    }

    public PasswordStrengthBar(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context);
    }

    public PasswordStrengthBar(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    protected void init(Context context) {
        LayoutScatter.getInstance(context).parse(
                ResourceTable.Layout_password_strength_bar, this, true);
        plb = (DirectionalLayout) findComponentById(ResourceTable.Id_progressLinearBar);
        pb1 = (ProgressBar) findComponentById(ResourceTable.Id_pBar1);
        pb2 = (ProgressBar) findComponentById(ResourceTable.Id_pBar2);
        pb3 = (ProgressBar) findComponentById(ResourceTable.Id_pBar3);
        pb4 = (ProgressBar) findComponentById(ResourceTable.Id_pBar4);
        setMaxStrength(mMax);
    }

    /**
     * Implement the below method to set the password strength to default chosen color
     *
     * @param noStrengthColor Color
     * @param color1          Color
     * @param color2          Color
     * @param color3          Color
     * @param color4          Color
     */
    public void setStrengthColor(int noStrengthColor, int color1, int color2, int color3, int color4) {
        this.mNoStrengthColor = noStrengthColor;
        this.mStrengthColor1 = color1;
        this.mStrengthColor2 = color2;
        this.mStrengthColor3 = color3;
        this.mStrengthColor4 = color4;
        pb1.setProgressColor(new Color(mStrengthColor1));
        pb2.setProgressColor(new Color(mStrengthColor2));
        pb3.setProgressColor(new Color(mStrengthColor3));
        pb4.setProgressColor(new Color(mStrengthColor4));
    }

    /**
     * Implement the below two methods to get the maximum
     * and minimum value to which the password strength can be calculated
     *
     * @return getMaxStrength
     */
    public int getMaxStrength() {
        return mMax;
    }

    public int getMinStrength() {
        return mMin;
    }

    /**
     * implement the below two methods to set the maximum
     * and minimum value to which the password strength can
     * be calculated
     *
     * @param max setMaxStrength
     */
    public void setMaxStrength(int max) {
        this.mMax = max;
        max /= 4;
        pb1.setMaxValue(max);
        pb2.setMaxValue(max);
        pb3.setMaxValue(max);
        pb4.setMaxValue(max);
    }

    public void setMinStrength(int min) {
        this.mMin = min;
        min /= 4;
        pb1.setMinValue(min);
        pb2.setMinValue(min);
        pb3.setMinValue(min);
        pb4.setMinValue(min);
    }

    /**
     * implement the below method to get the strength of the bar
     *
     * @return getStrength
     */
    public int getStrength() {
        return pb1.getProgress() + pb2.getProgress() + pb3.getProgress() + pb4.getProgress();
    }

    public int getmNoStrengthColor() {
        return mNoStrengthColor;
    }

    /**
     * Don't want complex ways to set the strength then use simple calculations
     * Below method to set the strength of the bar
     *
     * @param strength setStrength
     */
    public void setStrength(int strength) {
        if (strength <= mMin) {
            // set all the progress bar to its minimum value
            pb1.setProgressValue(mMin / 4);
            pb2.setProgressValue(mMin / 4);
            pb3.setProgressValue(mMin / 4);
            pb4.setProgressValue(mMin / 4);
        } else if (strength >= mMax) {
            // set all the progress bar to its maximum value
            pb1.setProgressValue(mMax / 4);
            pb2.setProgressValue(mMax / 4);
            pb3.setProgressValue(mMax / 4);
            pb4.setProgressValue(mMax / 4);
        } else {
            // set the progress bar accordingly
            if (strength - mMax / 4 >= mMin / 4) {
                pb1.setProgressValue(mMax / 4);
                pb2.setProgressValue(mMin);
                pb3.setProgressValue(mMin);
                pb4.setProgressValue(mMin);
                strength -= (mMax / 4);
                if (strength - mMax / 4 >= mMin / 4) {
                    pb2.setProgressValue(mMax / 4);
                    pb3.setProgressValue(mMin);
                    pb4.setProgressValue(mMin);
                    strength -= (mMax / 4);
                    if (strength - mMax / 4 >= mMin / 4) {
                        pb3.setProgressValue(mMax / 4);
                        pb4.setProgressValue(mMin);
                        strength -= (mMax / 4);
                        if (strength - mMax / 4 >= mMin / 4) {
                            pb4.setProgressValue(mMax / 4);
                        } else {
                            pb4.setProgressValue(strength);
                        }
                    } else {
                        pb3.setProgressValue(strength);
                        pb4.setProgressValue(mMin);
                    }
                } else {
                    pb2.setProgressValue(strength);
                    pb3.setProgressValue(mMin);
                    pb4.setProgressValue(mMin);
                }
            } else {
                pb1.setProgressValue(strength);
                pb2.setProgressValue(mMin);
                pb3.setProgressValue(mMin);
                pb4.setProgressValue(mMin);
            }
        }
    }
}
